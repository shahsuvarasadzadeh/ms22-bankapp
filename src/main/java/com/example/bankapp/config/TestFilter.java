package com.example.bankapp.config;


import com.example.bankapp.dto.ErrorResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

//@Component
@Slf4j
@RequiredArgsConstructor
public class TestFilter extends OncePerRequestFilter {
    public static final String BEARER = "Bearer ";
    private final JwtService jwtService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        String token = request.getHeader("Authorization");
        try {

            if (request.getRequestURI().equals("/bank-app/accounts/security/error")) {
//                throw new RuntimeException("Path incorrect");
            }

        } catch (Exception e) {

            e.printStackTrace();

            ErrorResponse errorResponse = new ErrorResponse();
            errorResponse.setFailCode(String.valueOf(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.value()));
            errorResponse.setFailMessage(e.getMessage());

//            response.getWriter().print(convertExpToObject(errorResponse));
//            response.setStatus(HttpStatus.BANDWIDTH_LIMIT_EXCEEDED.value());
//            return;
        }

        filterChain.doFilter(request, response);
    }

    public String convertExpToObject(Object object) throws JsonProcessingException {
        if (object == null) return null;
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }
}
