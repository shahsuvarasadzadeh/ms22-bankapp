package com.example.bankapp.service.impl;

import com.example.bankapp.dto.AccountDto;
import com.example.bankapp.entity.Account;
import com.example.bankapp.mapper.AccountMapper;
import com.example.bankapp.repository.AccountRepository;
import com.example.bankapp.service.AccountService;
import java.util.List;
import java.util.Random;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;
    private final AccountServiceImpl2 accountServiceImpl2;

    @Override
    public List<Account> getAccountDto() {
        return null;
    }

    @Override
    @Transactional
    public AccountDto getAccount(Long id) {
        System.err.println("Begin get : " + Thread.currentThread().getName());
        Account account = accountRepository.findById(id).orElseThrow();
        AccountDto accountDto = accountMapper.entityToDto(account);
        System.err.println("End get : " + Thread.currentThread().getName());
        return accountDto;
    }

    @Override
    @Transactional
    public AccountDto updateAccount(Long id, Double amount) {
        System.err.println("Begin update with NoWait : " + Thread.currentThread().getName());
        Account account = accountRepository.findById(id).orElseThrow();
        account.setBalance(account.getBalance() - amount);
        AccountDto accountDto = accountMapper.entityToDto(account);
        accountRepository.save(account);
        System.err.println("End update with NoWait : " + Thread.currentThread().getName());
        return accountDto;
    }

    @Override
    @SneakyThrows
    @Transactional
    public AccountDto updateAccountWithWait(Long id, Double amount) {
        System.err.println("Begin update with wait : " + Thread.currentThread().getName());
        Account account = accountRepository.findById(id).orElseThrow();
        account.setBalance(account.getBalance() - amount);
        Thread.sleep(5000);
        AccountDto accountDto = accountMapper.entityToDto(account);
        accountRepository.save(account);
        System.err.println("End update with wait : " + Thread.currentThread().getName());
        return accountDto;
    }


    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public AccountDto updateAccountWithPropagation(Long id, Double amount) {
        log.info("Method first");
        amount += amount;
        AccountDto accountDto = accountServiceImpl2.updateAccountWithPropagation2(id, amount);
        Random random = new Random();
        boolean con = random.nextBoolean();
        System.out.println("Con : " + con);
        if (con) {
            throw new RuntimeException("");
        }
        return accountDto;
    }


}
