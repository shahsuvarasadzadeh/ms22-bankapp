package com.example.bankapp.service.impl;

import com.example.bankapp.dto.AccountDto;
import com.example.bankapp.entity.Account;
import com.example.bankapp.mapper.AccountMapper;
import com.example.bankapp.repository.AccountRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountServiceImpl2 {

    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;

    @Transactional(propagation = Propagation.REQUIRED)
    public AccountDto updateAccountWithPropagation2(Long id, Double amount) {
        log.info("Method second");
        Account account = accountRepository.findById(1L).orElseThrow();
        account.setBalance(amount);
        return accountMapper.entityToDto(account);
    }
}
