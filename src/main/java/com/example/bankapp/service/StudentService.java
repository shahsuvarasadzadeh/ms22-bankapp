package com.example.bankapp.service;

import com.example.bankapp.dto.SearchCriteria;
import com.example.bankapp.entity.Student;
import java.util.List;
import org.springframework.data.domain.Page;

public interface StudentService {
    Long saveStudent(Student student);

    List<Student> getAllStudents(Student student);

    List<Student> getStudents(String name, String surname, Long age, String gender);

    List<Student> findAllStudents(List<SearchCriteria> searchCriteriaList);

    Page<Student> getStudentAll(int pageSize, int pageNumber, String[] pageSort);

    Student updateStudent(Student student, Long id);
}
