package com.example.bankapp.controller;

import com.example.bankapp.config.JwtService;
import com.example.bankapp.dto.AccountDto;
import com.example.bankapp.entity.Account;
import com.example.bankapp.entity.User;
import com.example.bankapp.service.AccountService;
import com.example.bankapp.service.impl.UserDetailsServiceImpl;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.security.Principal;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/accounts")
@RequiredArgsConstructor
@Tag(name = "Account controller", description = "This controller managed account")
@Slf4j
public class AccountController {
    private final AccountService accountService;
    private final JwtService jwtService;
    private final UserDetailsService userDetailsService;
    private final AuthenticationManager authenticationManager;

    public List<Account> getAccountDto() {
        return accountService.getAccountDto();
    }


    @GetMapping
    public AccountDto getAccount(@PathVariable Long id) {
        return accountService.getAccount(id);
    }

    @PutMapping("/{id}")

    public AccountDto updateAccount(@Parameter(example = "2") @PathVariable Long id,
                                    @Parameter(example = "400") @RequestParam Double amount) {
        return accountService.updateAccount(id, amount);
    }

    @PutMapping("/wait/{id}")
    public AccountDto updateAccountWithWait(@PathVariable Long id,
                                            @RequestParam Double amount) {
        return accountService.updateAccountWithWait(id, amount);
    }


    @PutMapping("/propagation/{id}")
    public AccountDto updateAccountWithPropagation(@PathVariable Long id,
                                                   @RequestParam Double amount) {
        return accountService.updateAccountWithPropagation(id, amount);
    }


    @GetMapping("/security")
    public String getPublic() {
        UserDetails userDetails = userDetailsService.loadUserByUsername("qulu");
        String generateToken = jwtService.generateToken((User) userDetails);
        return "token: " + generateToken;
    }

    @GetMapping("/security/1")
    @PreAuthorize("#principal.name == 'qulu'")
    public String getPublic1(Principal principal) {
        log.info("User is : {}", principal.getName());
        return "get public method 1";
    }

    @GetMapping("/security/1/2")
    public String getPublic2() {
        return "get public method 1/2";
    }

    @PostMapping("/security")
    public String postPublic() {
        Authentication authentication =
                authenticationManager
                        .authenticate(new UsernamePasswordAuthenticationToken("qulu", "123"));
        return "post public method";
    }
}
