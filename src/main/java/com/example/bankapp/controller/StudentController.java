package com.example.bankapp.controller;

import com.example.bankapp.dto.SearchCriteria;
import com.example.bankapp.entity.Student;
import com.example.bankapp.repository.StudentRepository;
import com.example.bankapp.service.StudentService;
import java.util.List;
import java.util.Objects;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class StudentController {

    private final StudentRepository studentRepository;
    private final StudentService studentService;
    private final CacheManager cacheManager;
//    private final RedisTemplate<String, Object> redisTemplate;

    //    @Cacheable(cacheNames = "student", key = "#name.length()")
    @GetMapping("/student/{name}")
    public Student getStudent(@PathVariable String name) {
//        Student student = studentRepository.findByName(name).orElseThrow(RuntimeException::new);
//        redisTemplate.opsForValue().set("student", student);
        Cache cache = cacheManager.getCache("student");
        Student cacheStudent = Objects.requireNonNull(cache).get(name, Student.class);
        if (cacheStudent == null) {
            Student student = studentRepository.findByName(name).orElseThrow(RuntimeException::new);
            cache.put(student.getName(), student);
            return student;
        }
        return (Student) cache.get(name).get();
    }

    @PutMapping("/student/{id}")
    public Student updateStudent(@RequestBody Student student, @PathVariable Long id) {
        return studentService.updateStudent(student, id);
    }

    @CacheEvict(cacheNames = "student", key = "#name.length()")
    @DeleteMapping("/student/{id}/{name}")
    public Long deleteStudent(@PathVariable Long id,
                              @PathVariable String name) {
        Student student = studentRepository.findById(id).orElseThrow(RuntimeException::new);
        studentRepository.delete(student);
        return student.getId();
    }

    @PostMapping("/student")
    public ResponseEntity<List<Student>> findAllStudents(@RequestBody List<SearchCriteria> searchCriteriaList) {
        return ResponseEntity.ok(studentService.findAllStudents(searchCriteriaList));
    }

    @GetMapping("/student/all")
    public ResponseEntity<Page<Student>> getStudentAll(@RequestParam(value = "pageSize") int pageSize,
                                                       @RequestParam(value = "pageNumber") int pageNumber,
                                                       @RequestParam(value = "pageSort") String[] pageSort) {
        return ResponseEntity.ok(studentService.getStudentAll(pageSize, pageNumber, pageSort));
    }


}
